import com.fasterxml.jackson.databind.ObjectMapper;
import com.lb.endpoint.storelocator.model.AddressInfo;
import com.lb.endpoint.storelocator.model.Day;
import com.lb.endpoint.storelocator.model.GeoLocationInfo;
import com.lb.endpoint.storelocator.model.OperationalHours;
import com.lb.endpoint.storelocator.model.Store;
import com.lb.endpoint.storelocator.model.StoreData;
import com.lb.endpoint.storelocator.model.StoreInfo;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StoreVOJsonTest {

  @Ignore
  @Test
  public void testStoreVO() {
    Set<StoreData> stores = new HashSet<>();

    // Store #1
    Store store = new Store();

    store.setStoreId(734);
    store.setBrandId("VSS");

    // AddressInfo
    AddressInfo addressInfo = new AddressInfo();
    addressInfo.setStreetAddress("7 Lincoln PL");
    addressInfo.setCity("Columbus");
    addressInfo.setState("OH");
    addressInfo.setPostalCode("43211");
    addressInfo.setCountry("US");
    addressInfo.setPhone("(614) 833-7890");
    addressInfo.setMallName("Polaris Parkway");
    store.setAddressInfo(addressInfo);

    // StoreInfo
    StoreInfo storeInfo = new StoreInfo();
    storeInfo.setStoreStatus("01");
    storeInfo.setStoreType("06");
    storeInfo.setOpeningDate(new Date());
    storeInfo.setRemodelDate(new Date());
    storeInfo.setClosingDate(new Date());
    store.setStoreInfo(storeInfo);

    // GeoLocationInfo
    GeoLocationInfo geoLocationInfo = new GeoLocationInfo();
    geoLocationInfo.setLatitudeDeg(39.961180);
    geoLocationInfo.setLongitudeDeg(-82.998790);
    geoLocationInfo.setLatitudeRad(0.6974542);
    geoLocationInfo.setLongitudeRad(-1.448602161);
    store.setGeoLocationInfo(geoLocationInfo);

    // Assortments
    List<String> assortments = new ArrayList<>();
    assortments.add("BEAUTY");
    assortments.add("LINGERIE");
    assortments.add("PINK");
    assortments.add("SWIM");
    assortments.add("VSX");
    store.setAssortments(assortments);

    StoreData storeData = new StoreData();
    storeData.setStore(store);
    storeData.setOperationalHours(buildOperationalHours());
    stores.add(storeData);

    // Store #2
    store = new Store();

    store.setStoreId(1127);
    store.setBrandId("VSS");

    // AddressInfo
    addressInfo = new AddressInfo();
    addressInfo.setStreetAddress("4949 Mall Drive");
    addressInfo.setCity("Honolulu");
    addressInfo.setState("HI");
    addressInfo.setPostalCode("96818");
    addressInfo.setCountry("US");
    addressInfo.setPhone("(808) 478-8000");
    addressInfo.setMallName("Island Paradise");
    store.setAddressInfo(addressInfo);

    // StoreInfo
    storeInfo = new StoreInfo();
    storeInfo.setStoreStatus("04");
    storeInfo.setStoreType("09");
    storeInfo.setOpeningDate(new Date());
    storeInfo.setRemodelDate(null);
    storeInfo.setClosingDate(new Date());
    store.setStoreInfo(storeInfo);

    // GeoLocationInfo
    geoLocationInfo = new GeoLocationInfo();
    geoLocationInfo.setLatitudeDeg(21.315603);
    geoLocationInfo.setLongitudeDeg(-157.858093);
    geoLocationInfo.setLatitudeRad(0.3720275);
    geoLocationInfo.setLongitudeRad(-2.755143474);
    store.setGeoLocationInfo(geoLocationInfo);

    // Assortments
    assortments = new ArrayList<>();
    assortments.add("BEAUTY");
    assortments.add("LINGERIE");
    assortments.add("SWIM");
    store.setAssortments(assortments);

    storeData = new StoreData();
    storeData.setStore(store);
    storeData.setOperationalHours(buildOperationalHours());
    stores.add(storeData);

    // Generate JSON
    getJsonSchema(stores);
  }

  private List<OperationalHours> buildOperationalHours() {
    List<OperationalHours> operationalHourses = new ArrayList<>();
    OperationalHours operationalHours = new OperationalHours();

    Calendar openTime  = Calendar.getInstance();
    openTime.set(2016, 06, 10, 8, 0, 0);

    Calendar closeTime = Calendar.getInstance();
    closeTime.setTime(openTime.getTime());
    closeTime.add(Calendar.HOUR_OF_DAY, 10);

    // Sunday
    operationalHours.setOpenTime(openTime.getTime());
    operationalHours.setCloseTime(closeTime.getTime());
    operationalHours.setOpen(true);
    operationalHours.setWeekDay(Day.SUNDAY);
    operationalHourses.add(operationalHours);

    // Monday
    openTime.add(Calendar.DAY_OF_MONTH, 1);
    closeTime.add(Calendar.DAY_OF_MONTH, 1);

    operationalHours = new OperationalHours();
    operationalHours.setOpenTime(openTime.getTime());
    operationalHours.setCloseTime(closeTime.getTime());
    operationalHours.setOpen(true);
    operationalHours.setWeekDay(Day.MONDAY);
    operationalHourses.add(operationalHours);

    // Tuesday
    openTime.add(Calendar.DAY_OF_MONTH, 1);
    closeTime.add(Calendar.DAY_OF_MONTH, 1);

    operationalHours = new OperationalHours();
    operationalHours.setOpenTime(openTime.getTime());
    operationalHours.setCloseTime(closeTime.getTime());
    operationalHours.setOpen(true);
    operationalHours.setWeekDay(Day.TUESDAY);
    operationalHourses.add(operationalHours);

    // Wednesday
    openTime.add(Calendar.DAY_OF_MONTH, 1);
    closeTime.add(Calendar.DAY_OF_MONTH, 1);

    operationalHours = new OperationalHours();
    operationalHours.setOpenTime(openTime.getTime());
    operationalHours.setCloseTime(closeTime.getTime());
    operationalHours.setOpen(true);
    operationalHours.setWeekDay(Day.WEDNESDAY);
    operationalHourses.add(operationalHours);

    // Thursday
    openTime.add(Calendar.DAY_OF_MONTH, 1);
    closeTime.add(Calendar.DAY_OF_MONTH, 1);

    operationalHours = new OperationalHours();
    operationalHours.setOpenTime(openTime.getTime());
    operationalHours.setCloseTime(closeTime.getTime());
    operationalHours.setOpen(true);
    operationalHours.setWeekDay(Day.THURSDAY);
    operationalHourses.add(operationalHours);

    // Friday
    openTime.add(Calendar.DAY_OF_MONTH, 1);
    closeTime.add(Calendar.DAY_OF_MONTH, 1);

    operationalHours = new OperationalHours();
    operationalHours.setOpenTime(openTime.getTime());
    operationalHours.setCloseTime(closeTime.getTime());
    operationalHours.setOpen(true);
    operationalHours.setWeekDay(Day.FRIDAY);
    operationalHourses.add(operationalHours);

    // Saturday
    openTime.add(Calendar.DAY_OF_MONTH, 1);
    closeTime.add(Calendar.DAY_OF_MONTH, 1);

    operationalHours = new OperationalHours();
    operationalHours.setOpenTime(openTime.getTime());
    operationalHours.setCloseTime(closeTime.getTime());
    operationalHours.setOpen(true);
    operationalHours.setWeekDay(Day.SATURDAY);
    operationalHourses.add(operationalHours);

    return operationalHourses;
  }

  private static void getJsonSchema(Set<StoreData> stores) {
    ObjectMapper mapper = new ObjectMapper();

    String jsonInString = null;
    try {
      jsonInString = mapper.writeValueAsString(stores);
    } catch (Exception ex) {
      System.out.println("ex = " + ex.getMessage());
    }

    System.out.println(jsonInString);
  }

}
