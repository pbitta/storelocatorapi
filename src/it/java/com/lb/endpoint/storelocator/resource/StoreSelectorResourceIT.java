package com.lb.endpoint.storelocator.resource;

import com.lb.endpoint.storelocator.model.Stores;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class StoreSelectorResourceIT {


  @BeforeClass
  public static void setup(){
    System.setProperty("groupName","ket05");
  }

  @Test
  @Ignore
  public void testSearchStoresByKeyword() throws Exception {
    StoreLocatorResource resource = new StoreLocatorResource();
    Response response  = resource.searchStoresByKeyword("33071");
    assertEquals(response.getStatus(), 200);
  }

  @Test
  @Ignore
  public void testSearchStoresByGeoLoc() throws Exception {
    StoreLocatorResource resource = new StoreLocatorResource();
    Response response  = resource.searchStoresByGeoLoc(21.315603, -157.858093, 75.0);
    assertEquals(response.getStatus(), 200);
  }

  @Test
  @Ignore
  public void testFindStoresByStoreId() throws Exception {
    StoreLocatorResource resource = new StoreLocatorResource();
    List<String> storeIds = new ArrayList<>();
    storeIds.add("754");
    storeIds.add("1175");

    Stores stores = new Stores();
    stores.setStoreIds(storeIds);

    Response response  = resource.findStoresByStoreId(stores);
    assertEquals(response.getStatus(), 200);
  }

  @Test
  @Ignore
  public void testFindStoresByStoreIdNonNumeric() throws Exception {
    StoreLocatorResource resource = new StoreLocatorResource();
    List<String> storeIds = new ArrayList<>();
    storeIds.add("77");
    storeIds.add("97j");     // Invalid storeId - non-numeric
    storeIds.add("1080");

    Stores stores = new Stores();
    stores.setStoreIds(storeIds);

    Response response  = resource.findStoresByStoreId(stores);

    // Validation
    // Service will continue to execute, processing those storeIds that are
    // valid.  Only logging the invalid storeIds.
    assertEquals(response.getStatus(), 200);
  }
}
