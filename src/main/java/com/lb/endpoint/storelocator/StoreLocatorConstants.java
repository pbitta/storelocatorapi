package com.lb.endpoint.storelocator;

/**
 * Constants for StoreLocator
 */
public interface StoreLocatorConstants {

  String REQUEST_ID = "requestId";

  String END_STORE_LOCATOR_UNKNOWN = "END-SL-UNKNOWN_ERROR";
  String END_STORE_LOCATOR_UNKNOWN_MESSAGE = "Unknown Error Occurred!";

  String END_STORE_LOCATOR_WEBAPP = "END-SL-WEBAPP_EXCEPTION";
  String END_STORE_LOCATOR_WEBAPP_MESSAGE = "Web App Exception Occurred!";

  //All Events
  String EVENT_GET_BY_KEYWORD = "GET_BY_KEYWORD";
  String EVENT_GET_BY_GEOLOCATION = "GET_BY_GEOLOCATION";
  String EVENT_GET_BY_STORE_IDS = "GET_BY_STORE_IDS";

  String DEFAULT_RADIUS = "50.0";
}
