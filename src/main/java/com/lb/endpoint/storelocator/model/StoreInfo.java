package com.lb.endpoint.storelocator.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.Objects;


/**
 * StoreInfo
 */
public class StoreInfo {

  private String storeType = null;
  private String storeStatus = null;
  private Date openingDate = null;
  private Date closingDate = null;
  private Date remodelDate = null;


  @JsonProperty("storeType")
  public String getStoreType() {
    return storeType;
  }

  public void setStoreType(String storeType) {
    this.storeType = storeType;
  }


  @JsonProperty("storeStatus")
  public String getStoreStatus() {
    return storeStatus;
  }

  public void setStoreStatus(String storeStatus) {
    this.storeStatus = storeStatus;
  }


  @JsonProperty("openingDate")
  public Date getOpeningDate() {
    return openingDate;
  }

  public void setOpeningDate(Date openingDate) {
    this.openingDate = openingDate;
  }


  @JsonProperty("closingDate")
  public Date getClosingDate() {
    return closingDate;
  }

  public void setClosingDate(Date closingDate) {
    this.closingDate = closingDate;
  }


  @JsonProperty("remodelDate")
  public Date getRemodelDate() {
    return remodelDate;
  }

  public void setRemodelDate(Date remodelDate) {
    this.remodelDate = remodelDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StoreInfo storeInfo = (StoreInfo) o;
    return Objects.equals(this.storeType, storeInfo.storeType) &&
            Objects.equals(this.storeStatus, storeInfo.storeStatus) &&
            Objects.equals(this.openingDate, storeInfo.openingDate) &&
            Objects.equals(this.closingDate, storeInfo.closingDate) &&
            Objects.equals(this.remodelDate, storeInfo.remodelDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(storeType, storeStatus, openingDate, closingDate, remodelDate);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StoreInfo {\n");

    sb.append("    storeType: ").append(toIndentedString(storeType)).append("\n");
    sb.append("    storeStatus: ").append(toIndentedString(storeStatus)).append("\n");
    sb.append("    openingDate: ").append(toIndentedString(openingDate)).append("\n");
    sb.append("    closingDate: ").append(toIndentedString(closingDate)).append("\n");
    sb.append("    remodelDate: ").append(toIndentedString(remodelDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

