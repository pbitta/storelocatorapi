package com.lb.endpoint.storelocator.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.Objects;


/**
 * OperationalHours
 */
public class OperationalHours {

  private Date openTime = null;
  private String openTimeDisplay = null;
  private Date closeTime = null;
  private String closeTimeDisplay = null;
  private Boolean open = null;
  private Day weekDay = null;


  /**
   **/
  public OperationalHours openTime(Date openTime) {
    this.openTime = openTime;
    return this;
  }

  @JsonProperty("openTime")
  public Date getOpenTime() {
    return openTime;
  }

  public void setOpenTime(Date openTime) {
    this.openTime = openTime;
  }

  @JsonProperty("openTimeDisplay")
  public String getOpenTimeDisplay() {
    return openTimeDisplay;
  }

  public void setOpenTimeDisplay(String openTimeDisplay) {
    this.openTimeDisplay = openTimeDisplay;
  }


  /**
   **/
  public OperationalHours closeTime(Date closeTime) {
    this.closeTime = closeTime;
    return this;
  }

  @JsonProperty("closeTime")
  public Date getCloseTime() {
    return closeTime;
  }

  public void setCloseTime(Date closeTime) {
    this.closeTime = closeTime;
  }

  @JsonProperty("closeTimeDisplay")
  public String getCloseTimeDisplay() {
    return closeTimeDisplay;
  }

  public void setCloseTimeDisplay(String closeTimeDisplay) {
    this.closeTimeDisplay = closeTimeDisplay;
  }


  /**
   **/
  public OperationalHours open(Boolean open) {
    this.open = open;
    return this;
  }

  @JsonProperty("open")
  public Boolean getOpen() {
    return open;
  }

  public void setOpen(Boolean open) {
    this.open = open;
  }


  /**
   **/
  public OperationalHours weekDay(Day weekDay) {
    this.weekDay = weekDay;
    return this;
  }

  @JsonProperty("weekDay")
  public Day getWeekDay() {
    return weekDay;
  }

  public void setWeekDay(Day weekDay) {
    this.weekDay = weekDay;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OperationalHours operationalHours = (OperationalHours) o;
    return Objects.equals(this.openTime, operationalHours.openTime) &&
            Objects.equals(this.openTimeDisplay, operationalHours.openTimeDisplay) &&
            Objects.equals(this.closeTime, operationalHours.closeTime) &&
            Objects.equals(this.closeTimeDisplay, operationalHours.closeTimeDisplay) &&
            Objects.equals(this.open, operationalHours.open) &&
            Objects.equals(this.weekDay, operationalHours.weekDay);
  }

  @Override
  public int hashCode() {
    return Objects.hash(openTime, openTimeDisplay, closeTime, closeTimeDisplay, open, weekDay);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OperationalHours {\n");

    sb.append("    openTime: ").append(toIndentedString(openTime)).append("\n");
    sb.append("    openTimeDisplay: ").append(toIndentedString(openTimeDisplay)).append("\n");
    sb.append("    closeTime: ").append(toIndentedString(closeTime)).append("\n");
    sb.append("    closeTimeDisplay: ").append(toIndentedString(closeTimeDisplay)).append("\n");
    sb.append("    open: ").append(toIndentedString(open)).append("\n");
    sb.append("    weekDay: ").append(toIndentedString(weekDay)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

