package com.lb.endpoint.storelocator.exceptions;

import com.lb.cache.auditmetrics.AuditMetricsAPI;
import com.lb.endpoint.storelocator.StoreLocatorConstants;
import com.lb.util.serviceutils.exception.Error;
import com.lb.util.serviceutils.utils.AuditMetricsLogger;
import com.lb.util.serviceutils.utils.ErrorLoggingUtil;
import org.glassfish.jersey.server.ContainerRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Exception Handler for the Resources
 */
@Provider
public class GenericExceptionHandler implements ExceptionMapper<Throwable> {


  @Inject
  private javax.inject.Provider<ContainerRequest> containerRequestProvider;

    private static Logger LOGGER = LoggerFactory.getLogger(GenericExceptionHandler.class);

  @Override
  public Response toResponse(Throwable throwable) {
      LOGGER.error("Generic Exception", throwable);

    if (throwable instanceof WebApplicationException) {
      Response.Status webAppStatus =
              Response.Status.fromStatusCode(((WebApplicationException) throwable).getResponse().getStatus());

      return auditAndRespond(throwable, webAppStatus, StoreLocatorConstants.END_STORE_LOCATOR_WEBAPP,
              StoreLocatorConstants.END_STORE_LOCATOR_WEBAPP_MESSAGE);
    } else {
      return auditAndRespond(throwable, Response.Status.INTERNAL_SERVER_ERROR,
              StoreLocatorConstants.END_STORE_LOCATOR_UNKNOWN,
              StoreLocatorConstants.END_STORE_LOCATOR_UNKNOWN_MESSAGE);
    }
  }

  private Response auditAndRespond(Throwable exception, Response.Status status, String appErrorCode, String appErrorMsg) {
    try {

        if (exception instanceof WebApplicationException) {
            //dont log
        }else{
            AuditMetricsLogger.logAudit(ErrorLoggingUtil.
                    createServiceAuditMetrics(exception, appErrorCode, containerRequestProvider));
        }


    } catch (Throwable t) {
        LOGGER.error("Audit Metrics Failed for " +
              "GenericExceptionHandler  " + containerRequestProvider.get().getAbsolutePath(), t, exception);
    }

      return Response.status(status)
              .entity(new Error(appErrorCode, appErrorMsg))
            .type(MediaType.APPLICATION_JSON).build();

  }
}
