package com.lb.endpoint.storelocator.resource;


import com.lb.endpoint.storelocator.StoreLocatorConstants;
import com.lb.endpoint.storelocator.model.AddressInfo;
import com.lb.endpoint.storelocator.model.Day;
import com.lb.endpoint.storelocator.model.GeoLocationInfo;
import com.lb.endpoint.storelocator.model.OperationalHours;
import com.lb.endpoint.storelocator.model.Store;
import com.lb.endpoint.storelocator.model.StoreData;
import com.lb.endpoint.storelocator.model.StoreInfo;
import com.lb.endpoint.storelocator.model.Stores;
import com.lb.service.storelocator.constants.StoreLocatorServiceConstants;
import com.lb.service.storelocator.exceptions.BadRequestException;
import com.lb.service.storelocator.impl.StoreLocatorServiceImpl;
import com.lb.util.serviceutils.utils.AuditMetricsLogger;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.lb.endpoint.storelocator.StoreLocatorConstants.DEFAULT_RADIUS;

/**
 * WebResources for store locator. Returns content-type as JSON. API versions
 * are maintained through restful endpoints.
 */
@Path("/VS/v1/stores")
@Produces(MediaType.APPLICATION_JSON)
public class StoreLocatorResource {

  private static Logger LOGGER = LoggerFactory.getLogger(StoreLocatorResource.class);

  /**
   * Search for stores based on a keyword.
   *
   * @param keyword - may be any of:  postalCode, City, State, Mall Name
   * @return Set of Stores.
   */
  @GET
  @Path("/search")
  public Response searchStoresByKeyword(@QueryParam(value = "keyword") String keyword) {

    AuditMetricsLogger.logMetrics(StoreLocatorConstants.EVENT_GET_BY_KEYWORD, keyword, StringUtils.EMPTY);

    Set<com.lb.service.storelocator.model.StoreData> serviceStores =
            StoreLocatorServiceImpl.getInstance().getStoresBySearchKeyword(keyword);

    return Response.ok(copyServiceModelToEndpointModel(serviceStores)).build();
  }

  /**
   * Search for stores of a specific radius around the longitude and latitude.
   *
   * @param latitude  latitude of the location
   * @param longitude longitude of the location
   * @param radius    radius around the point
   * @return Set of Stores
   */
  @GET
  @Path("/search/geolocation")
  public Response searchStoresByGeoLoc(@QueryParam(value = "lat") Double latitude,
                                       @QueryParam(value = "long") Double longitude,
                                       @QueryParam(value = "radius") @DefaultValue(DEFAULT_RADIUS) Double radius) {
    StringBuilder queryParams = new StringBuilder();
    queryParams.append("lat: ").append(latitude).
            append(", long: ").append(longitude).
            append(", radius: ").append(radius);

    AuditMetricsLogger.logMetrics(
            StoreLocatorConstants.EVENT_GET_BY_GEOLOCATION,
            queryParams.toString(),
            StringUtils.EMPTY);

    Set<com.lb.service.storelocator.model.StoreData> serviceStores =
            StoreLocatorServiceImpl.getInstance().getStoresByGeoLocation(latitude, longitude, radius);

    return Response.ok(copyServiceModelToEndpointModel(serviceStores)).build();
  }

  /**
   * Find stores by store Id.
   *
   * @param stores A set of store Ids
   * @return Set of Store details
   */
  @POST
  @Path("/find")
  public Response findStoresByStoreId(Stores stores) {
    if (stores == null || stores.getStoreIds() == null) {
      Map<String, Object> errData = new HashMap<>();
      errData.put("stores", stores);
      throw new BadRequestException(StoreLocatorServiceConstants.SL_INPUT_INVALID_CODE,
              StoreLocatorServiceConstants.INPUT_INVALID_MESSAGE,
              errData);
    }

    AuditMetricsLogger.logMetrics(
            StoreLocatorConstants.EVENT_GET_BY_STORE_IDS,
            stores.getStoreIds(),
            StringUtils.EMPTY);

    Set<com.lb.service.storelocator.model.StoreData> serviceStores =
            StoreLocatorServiceImpl.getInstance().
                    getStoresByStoreId(convertStoreIdsToInts(new HashSet<>(stores.getStoreIds())));

    return Response.ok(copyServiceModelToEndpointModel(serviceStores)).build();
  }

  /**
   * Converts a Set of String storeIds to a Set of Ints
   *
   * @param storeIds a Set of Strings
   *
   * @return a Set of Ints
   */
  private Set<Integer> convertStoreIdsToInts(Set<String> storeIds) {
    Set<Integer> stores = new HashSet<>();

    storeIds.stream().forEach(i -> {
      try {
        stores.add(Integer.parseInt(i));
      } catch (NumberFormatException nfe) {
        // This should never happen for this service since all storeIds for VS are numeric.
        // However, if it does occur, keep processing and return data for those storeIds
        // that are valid.  Simply log the invalid storeId.
        LOGGER.error(String.format("StoreId:  {} is invalid.  Must be numeric for 'VS'", i));
      }
    });

    return stores;
  }

  // Copy Service model Data to Endpoint model
  private Set<StoreData> copyServiceModelToEndpointModel(Set<com.lb.service.storelocator.model.StoreData> serviceStores) {
    return serviceStores.stream().map((serviceStore) -> {
      Store store = new Store();
      store.setStoreId(serviceStore.getStore().getStoreId());
      store.setBrandId(serviceStore.getStore().getBrandId());

      // Build Store info
      StoreInfo storeInfo = new StoreInfo();
      com.lb.service.storelocator.model.StoreInfo serviceStoreInfo = serviceStore.getStore().getStoreInfo();
      if (serviceStoreInfo != null) {
        storeInfo.setStoreType(serviceStoreInfo.getStoreType());
        storeInfo.setStoreStatus(serviceStoreInfo.getStoreStatus());
        storeInfo.setOpeningDate(serviceStoreInfo.getOpeningDate());
        storeInfo.setRemodelDate(serviceStoreInfo.getRemodelDate());
        storeInfo.setClosingDate(serviceStoreInfo.getClosingDate());
      }
      store.setStoreInfo(storeInfo);

      // Build Address info
      AddressInfo addressInfo = new AddressInfo();
      com.lb.service.storelocator.model.AddressInfo serviceAddressInfo = serviceStore.getStore().getAddressInfo();
      if (serviceAddressInfo != null) {
        addressInfo.setStreetAddress(serviceAddressInfo.getStreetAddress());
        addressInfo.setCity(serviceAddressInfo.getCity());
        addressInfo.setState(serviceAddressInfo.getState());
        addressInfo.setPostalCode(serviceAddressInfo.getPostalCode());
        addressInfo.setCountry(serviceAddressInfo.getCountry());
        addressInfo.setPhone(serviceAddressInfo.getPhone());
        addressInfo.setMallName(serviceAddressInfo.getMallName());
      }
      store.setAddressInfo(addressInfo);

      // Build GeoLocation info
      GeoLocationInfo geoLocationInfo = new GeoLocationInfo();
      com.lb.service.storelocator.model.GeoLocationInfo serviceGeoLocationInfo = serviceStore.getStore().getGeoLocationInfo();
      if (serviceGeoLocationInfo != null) {
        geoLocationInfo.setLatitudeDeg(serviceGeoLocationInfo.getLatitudeDeg());
        geoLocationInfo.setLongitudeDeg(serviceGeoLocationInfo.getLongitudeDeg());
        geoLocationInfo.setLatitudeRad(serviceGeoLocationInfo.getLatitudeRad());
        geoLocationInfo.setLongitudeRad(serviceGeoLocationInfo.getLongitudeRad());
      }
      store.setGeoLocationInfo(geoLocationInfo);

      store.setAssortments(serviceStore.getStore().getAssortments());

      // Populate Operational Hours
      List<OperationalHours> operationalHourses = new ArrayList<>();

      serviceStore.getOperationalHours().stream().forEach(serviceOpHoursDay -> {
        OperationalHours endpointsOperationalHours = new OperationalHours();
        endpointsOperationalHours.setOpenTime(serviceOpHoursDay.getOpenTime());
        endpointsOperationalHours.setOpenTimeDisplay(serviceOpHoursDay.getOpenTimeDisplay());
        endpointsOperationalHours.setCloseTime(serviceOpHoursDay.getCloseTime());
        endpointsOperationalHours.setCloseTimeDisplay(serviceOpHoursDay.getCloseTimeDisplay());
        endpointsOperationalHours.setOpen(serviceOpHoursDay.getOpen());
        endpointsOperationalHours.setWeekDay(Day.getDayByName(serviceOpHoursDay.getWeekDay().toString()));
        operationalHourses.add(endpointsOperationalHours);
      });

      StoreData storeData = new StoreData();
      storeData.setStore(store);
      storeData.setOperationalHours(operationalHourses);


      return storeData;
    }).collect(Collectors.toSet());
  }
}
