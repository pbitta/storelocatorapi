swagger: '2.0'
info:
  version: 1.0.0
  title: StoreLocator Service
  description: API that returns Store location information
host: ket-vscappdev.lbidts.com
schemes:
  - https
basePath: /storelocator
consumes:
  - application/json
produces:
  - application/json

paths:
  /VS/v1/stores/search:

    get:
      tags:
        - DEV
      description: |
        Search for stores based on a keyword.
      produces:
        - application/json
      parameters:
        - name: keyword
          in: query
          description: Search keyword.  For example, zip code, mall name, etc.
          required: true
          type: string

        - name: Cookies
          in: header
          description: VSSESSION cookie, if session needs to be kept alive
          type: string


      # Expected responses for this operation:
      responses:
        # Response code
        200:
          description: Successful response
          # A schema describing your response object.
          # Use JSON Schema format
          schema:
            title: SetOfStores
            type: array
            items:
              $ref: "#/definitions/StoreData"
        400:
          description: "Bad Request."
          schema:
              $ref: "#/definitions/Error"
        404:
          description: "No Stores Found."
          schema:
              $ref: "#/definitions/Error"
        default:
          description: "Unexpected Error - Internal Server or Web Application."
          schema:
              $ref: "#/definitions/Error"

  /VS/v1/stores/search/geolocation:
    get:
      tags:
        - DEV
      description: |
        Search for stores of a specific radius around the longitude and latitude.
      produces:
        - application/json
      parameters:
        - name: lat
          in: query
          description: latitude of the location
          required: true
          type: number
          format: double

        - name: long
          in: query
          description: longitude of the location
          required: true
          type: number
          format: double

        - name: radius
          in: query
          description: radius around the point
          type: number
          format: double

        - name: Cookies
          in: header
          description: VSSESSION cookie, if session needs to be kept alive
          type: string

      # Expected responses for this operation:
      responses:
        # Response code
        200:
          description: Successful response
          # A schema describing your response object.
          # Use JSON Schema format
          schema:
            title: SetOfStores
            type: array
            items:
              $ref: "#/definitions/StoreData"
        400:
          description: "Bad Request."
          schema:
              $ref: "#/definitions/Error"
        404:
          description: "No Stores Found."
          schema:
              $ref: "#/definitions/Error"
        default:
          description: "Unexpected Error - Internal Server or Web Application."
          schema:
              $ref: "#/definitions/Error"


  /VS/v1/stores/find:
    post:
      tags:
        - DEV
      description: |
        Find stores based on the store's Id
      produces:
        - application/json
      parameters:
        - name: Stores
          in: body
          description: List of store ids
          required: true
          schema:
            $ref: "#/definitions/Stores"

        - name: Cookies
          in: header
          description: VSSESSION cookie, if session needs to be kept alive
          type: string

      # Expected responses for this operation:
      responses:
        # Response code
        200:
          description: Successful response
          # A schema describing your response object.
          # Use JSON Schema format
          schema:
            title: SetOfStores
            type: array
            items:
              $ref: "#/definitions/StoreData"
        400:
          description: "Bad Request."
          schema:
              $ref: "#/definitions/Error"
        404:
          description: "No Stores Found."
          schema:
              $ref: "#/definitions/Error"
        default:
          description: "Unexpected Error - Internal Server or Web Application."
          schema:
              $ref: "#/definitions/Error"


definitions:
  StoreData:
    properties:
      store:
        $ref: "#/definitions/Store"
      operationalHours:
        type: array
        items:
          $ref: "#/definitions/OperationalHours"

  Store:
    properties:
      storeId:
        type: integer
        format: int32
      brandId:
        type: string
      storeInfo:
        $ref: "#/definitions/StoreInfo"
      addressInfo:
        $ref: "#/definitions/AddressInfo"
      geoLocationInfo:
        $ref: "#/definitions/GeoLocationInfo"
      assortments:
       type: array
       items:
        type: string


  StoreInfo:
    properties:
      storeType:
        type: string
      storeStatus:
        type: string
      openingDate:
        type: string
        format: date
      closingDate:
        type: string
        format: date
      remodelDate:
        type: string
        format: date


  GeoLocationInfo:
    properties:
      latitudeDeg:
        type: number
        format: double
      longitudeDeg:
        type: number
        format:  double
      latitudeRad:
        type: number
        format: double
      longitudeRad:
        type: number
        format: double


  AddressInfo:
    properties:
      streetAddress:
        type: string
      city:
        type: string
      state:
        type: string
      postalCode:
        type: string
      country:
        type: string
      phone:
        type: string
      mallName:
        type: string


  OperationalHours:
    properties:
      openTime:
        type: string
        format: date-time
      openTimeDisplay:
        type: string
      closeTime:
        type: string
        format: date-time
      closeTimeDisplay:
        type: string
      open:
        type: boolean
      weekDay:
        $ref: "#/definitions/Day"


  Day:
    type: "string"
    enum:
      ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"]

  Stores:
    type: "object"
    properties:
      storeIds:
        type: "array"
        items:
          type: "string"

  Error:
    type: "object"
    properties:
      errorCode:
        type: "string"
      errorMessage:
        type: "string"
