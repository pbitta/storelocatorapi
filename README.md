PROJECT

endpoint-storelocator

Find all the stores based on a given search criteria.

This project build a war - storelocator.war exposing the following endpoints:

1. http://localhost:8080/endpoint-storelocator/storelocator/v1/stores/search/geolocation?lat=-83.130469&long=40.074312&radius=100D


2. http://localhost:8080/endpoint-storelocator/storelocator/v1/stores/search?keyword=Tuttle

To build the project,

$ gradle clean build

Copy the storelocator.war into the webapps folder of Jetty or Use the embedded Jetty plugin (not configured in this project for now) and start the Jetty server.

To start the Jetty sever,
$ java -jar start.jar -DgroupName=cmh05

GroupName could be any of either (cmh05 or ket05)

You should login(via telnet) to the list of servers in this group  apart from the DR2 account to access the cache.

Application is accessible at the endpoints listed above. Endpoints API is as below:

1. {host}:{port}/endpoint-storelocator/storelocator/v1/stores/search/geolocation?lat={Latitude}&long={Longitude}&radius={Radius}

2. {host}:{port}/endpoint-storelocator/storelocator/v1/stores/search?keyword={Keyword(s) to search}


The proejct has a "jmeter.jmx" script.

The jmeter script tests various performance metrics for the endpoints listed above for a  200 status.

To run the jmeter script passing various parameters,

$ ./jmeter.sh -n -t <location to jmeter script> -Jhost=api.lbidts.com -Jport=8080 -Jusers=20 -Jrampup=1 -l <Name of the log file to save>

Example: ./jmeter.sh -n -t jmeter.jmx -l zzzzz.log -Jusers=20

If one or more parameters are not passed, jmeter uses the default values in the script



