#!/bin/bash

source setenv.sh

i_lower_case()
{
    echo $GROUP_NAME | tr 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' 'abcdefghijklmnopqrstuvwxyz'
}

env=/opt/local/share/app
jdk_bin=/opt/local/java/jdk8_current/bin/
groupName=`i_lower_case "${GROUP_NAME}"`

# create directory for logs
mkdir -p logs

# extract prod-logger.xml from endpt-nav jar
$jdk_bin/jar xf *.jar prod-logger.xml

nohup $jdk_bin/java -jar -DgroupName=$groupName -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.port=1099 -DClusteredSessions=${CLUSTERED_SESSIONS} -Dlog4j.configurationFile=File://$env/prod-logger.xml -Djetty.logs=$env/logs -Xmx4g -Xms4g *.jar > /opt/local/share/app/logs/jetty.log 2>&1 &
